
import 'package:dclost_app/components/component_goods_card.dart';
import 'package:dclost_app/pages/page_goods_detail.dart';
import 'package:dclost_app/repository/repo_goods.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../model/goods/goods_item.dart';

class PageGoodsList extends StatefulWidget {
  const PageGoodsList({super.key});

  @override
  State<PageGoodsList> createState() => _PageGoodsListState();

}


class _PageGoodsListState extends State<PageGoodsList> {

  List<GoodsItem> _list = [];

  /*Future<void> _loadList() async {
    await RepoGoods().getGoodsList()
        .then((res) => {
          setState(() {_list = res.list;})})
=======
        .catchError(err) => {debugPrint(err)})
  }*/

  Future<void> _loadList() async {
    await RepoGoods().getGoodsList()
        .then((res) =>
    {
      setState(() {
        if (res.code == 0) {
          _list = res.list;
        } else {
          Fluttertoast.showToast(msg: res.msg);
        }
      })
    });
  }

  @override
  void initState() {
    super.initState();

    // 상품목록조회
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('코디 리스트'),
          centerTitle: true,
        ),
        body:ListView.builder(
          itemCount: _list.length,
            itemBuilder: (BuildContext context, int idx){
            return ComponentGoodsCard(goodsItem: _list[idx],callback: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageGoodsDetail(id: _list[idx].id)));
            },);
            })
    );
  }
}