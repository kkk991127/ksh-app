import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/repository/repo_notice.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../model/notice/notice_item.dart';

class PageNotice extends StatefulWidget {
  const PageNotice({super.key});

  @override
  State<PageNotice> createState() => _PageNoticeState();
}

/// 공지사항 메인 페이지 입니다

// 공지사항 제목
class _PageNoticeState extends State<PageNotice> {
  List<NoticeItem> _list = [];

  Future<void> _loadList() async {

    await RepoNotice().getNoticeList()
        .then((res) => {
      setState(() {
        if ( res.code == 0 ){
          _list = res.list;
        } else {
          Fluttertoast.showToast(msg: res.msg);
        }
      })
    });
  }

  @override
  void initState(){
    super.initState();

    // 상품목록조회
    _loadList();

  }

  //  List<NoticeItem> noticeTitle = [
  // ' 설 연휴 배송 안내',
  // ' 설 연휴 반품 안내',
  // ' 입금 확인 안내 ',
  // ' 반품 안내',
  // ' 반품은 이러면 안돼요 ',
  // '',
  // ''
  // ];



// 상세 페이지 이미지 주소

  //  List<NoticeItem> noticeImage =[
  //   'assets/delivery.png',
  //   'assets/return.png',
  //   'assets/pay-notice.png',
  //   'assets/return-no.png',
  //   'assets/delivery.png',
  //   'assets/delivery.png',
  //   'assets/delivery.png',
  //
  // ];

// 공지사항 상세 페이지 내용

  // static List<NoticeItem> noticeContent = [
  //   '설 연휴 배송 2/8일 출고 마감이며'
  //       '2/13 (월) 부터 정상 배송입니다.',
  //   '설 연휴기간에는 고객 상담이 어렵습니다'
  //       '설 연휴 기간 택배사 배송량 증가로 수거 지연 될 수 있습니다'
  //       '반품 기간 내 도착되지 않더라도 반품 접수 일 확인 후 처리 예정이니'
  //       '안심하고 보내주시기 바랍니다.',
  //   '입금 확인 안내드립니다 !!',
  //   '반품 사유 자세한 안내 드립니다 !!',
  //   '-',
  //   '-',
  //   '-'
  // ];
  //
  // final List<NoticeItem>  = List.generate(
  //     noticeTitle.length,
  //         (index) => NoticeItem(noticeTitle[index], noticeImage[index], noticeContent[index]));


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ComponentAppbar(),
        drawer: ComponentDrawer(),
      body:ListView.builder(
          itemCount: _list.length,
          itemBuilder: (context, index){
            return Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
              color: Colors.white,
              child: ListTile(
                title: Text(_list[index].noticeTitle),
                // leading argument는 앱바에서 왼쪽에 아이콘 넣을때 사용했던 걔임
                //
                // onTap: () {
                //   // debugPrint(animalData[index].name);
                //   Navigator.of(context).push(MaterialPageRoute(
                //       builder: (context) =>
                //           const PageNoticeDetail(noticeResponse: null,)
                //     )
                //   );
                // }
              ),
            );
          }
      ),

    );
  }

}

