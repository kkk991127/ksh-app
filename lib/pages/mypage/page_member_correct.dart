
import 'package:dclost_app/components/component_appbar.dart';
import 'package:flutter/material.dart';

/**
 * 회원정보 수정 페이지 입니다.
 */

class PageMemberCorrect extends StatefulWidget {

  @override
  State<PageMemberCorrect> createState() => _PageMemberCorrect();
}


class _PageMemberCorrect extends State<PageMemberCorrect> {
  TextStyle style = TextStyle(fontSize: 20.0);
  final _formKey = GlobalKey<FormState>();
  final _key = GlobalKey<ScaffoldState>();
  var _isChecked = false;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      appBar: ComponentAppbar(),
      body: Form(
        key: _formKey,
        child: Center(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                child: Text(
                  ' 회원 정보 수정',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  style: style,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.supervised_user_circle),
                    labelText: "아이디",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  style: style, // TextFormField 스타일 주기
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.perm_identity_outlined),
                    labelText: "이름",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  obscureText: true, // 비밀번호 숫자 가려주는 옵션
                  style: style, // TextFormField 스타일 주기
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.lock),
                    labelText: "비밀번호",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  obscureText: true, // 비밀번호 숫자 가려주는 옵션
                  style: style, // TextFormField 스타일 주기
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.lock),
                    labelText: "비밀번호 확인",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  style: style,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.call), // 전화기 모양 아이콘
                    labelText: "연락처",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  style: style,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.map), // 지도 아이콘
                    labelText: "주소",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  style: style,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.map),
                    labelText: "상세주소",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),

              Container(
                margin: EdgeInsets.all(5),
                child: Column(
                  children: [
                    ElevatedButton(onPressed: (){

                      // 가입 완료 팝업창
                      showDialog(context: context, builder: ((context) => AlertDialog(
                        title: Text(
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 22,

                            ),'회원 정보 수정'),
                        content: Text('회원 정보 수정이 완료 되었습니다.'),
                        actions: [
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'ok'), child: Text(
                              style: TextStyle(
                                color: Colors.black87,
                                fontWeight: FontWeight.bold,
                              ),
                              'ok'),
                          ),
                          TextButton(onPressed: () => Navigator.pop(context, 'Cancel'), child: Text(
                              style: TextStyle(
                                color: Colors.black54,
                                fontWeight: FontWeight.bold,
                              ),
                              'cancel'),
                          ),
                        ],
                      )));
                    },

                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.black,
                        foregroundColor: Colors.white,
                      ),
                      child: Text('수정'),),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: Column(
                  children: [
                    ElevatedButton(onPressed: (){

                      // 가입취소 팝업 알림 창
                      showDialog(context: context, builder: ((context) => AlertDialog(
                        title: Text(
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 22,

                            ),'회원 정보 수정 '),
                        content: Text('수정을 취소하시겠습니까?'),
                        actions: [
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'ok'), child: Text(
                              style: TextStyle(
                                color: Colors.black87,
                                fontWeight: FontWeight.bold,
                              ),
                              'ok'),
                          ),
                          TextButton(onPressed: () => Navigator.pop(context, 'Cancel'), child: Text(
                              style: TextStyle(
                                color: Colors.black54,
                                fontWeight: FontWeight.bold,
                              ),
                              'cancel'),
                          ),
                        ],
                      )));
                    },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.black,
                        foregroundColor: Colors.white,
                      ),
                      child: Text('취소'),),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
