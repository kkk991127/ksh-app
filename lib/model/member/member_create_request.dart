class MemberCreateRequest{
  num id;
  String username;
  String password;
  String passwordRe;
  String memberName;
  String birthDay;
  String phoneNumber;
  String memberAddress;
  String memberDetailedAddress;
  num postCode;
  bool nYPersonalInfo;
  bool nYMarketing;

  MemberCreateRequest(this.id,this.username,this.memberName,this.password,this.passwordRe,
      this.birthDay,this.phoneNumber,this.memberAddress,this.memberDetailedAddress,
      this.postCode,this.nYPersonalInfo,this.nYMarketing);

  Map<String,dynamic>toJson() => {  //제이슨을 받아서 스트링으로

    'id': id,
    'username':username,
    'password':password,
    'passwordRe':passwordRe,
    'memberName':memberName,
    'birthDay':birthDay,
    'phoneNumber':phoneNumber,
    'memberAddress':memberAddress,
    'memberDetailedAddress':memberDetailedAddress,
    'postCode':postCode,
    'nYPersonalInfo':nYPersonalInfo,
    'nYMarketing':nYMarketing

  };









  }
