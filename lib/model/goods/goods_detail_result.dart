
import 'package:dclost_app/model/goods/goods_response.dart';

class GoodsDetailResult {
  String msg;
  num code;
  GoodsResponse data;

  GoodsDetailResult(this.msg,this.code,this.data);

  factory GoodsDetailResult.fromJson(Map<String,dynamic>json){
    return GoodsDetailResult(
        json['msg'],
        json['code'],
        GoodsResponse.fromJson(json['data']) // 맨위의 데이터로 바꿔줘
    );

  }
}