class QuestionItem{

  // Q&A 게시판 복수 R 리스트 보여줄 양식 폼.

   dynamic member;
   DateTime questionCreateDate;
   String questionTitle;
   String questionContent;
   String questionStatus;

  QuestionItem(
      this.member,
      this.questionContent,
      this.questionCreateDate,
      this.questionTitle,
      this.questionStatus);

  factory QuestionItem.fromJson(Map<String,dynamic>json){
    return QuestionItem(
    json['member'],
    json['questionContent'],
    json['questionCreateDate'],
    json['questionTitle'],
    json['questionStatus']
    );
   }

}