
import 'package:dclost_app/config/config_api.dart';
import 'package:dclost_app/model/notice/notice_detail_result.dart';
import 'package:dclost_app/model/notice/notice_list_result.dart';
import 'package:dio/dio.dart';

class RepoNotice {

  //공지사항 리스트
  Future<NoticeListResult> getNoticeList() async {

    Dio dio = Dio();

    String _baseUrl = '$apiUrl/NoticeBulletin/list/all'; //엔드포인트

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return NoticeListResult.fromJson(response.data);
  }

  //공지사항 상세 불러오기

  //상품 한개 불러오기 (단수 )
  Future<NoticeDetailResult> getNotice(num id) async {

    Dio dio = Dio();

    String _baseUrl = '$apiUrl/NoticeBulletin/detail/{id}';

    final response = await dio.get(
        _baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (state){
              return state == 200;
            }
        )
    );

    return NoticeDetailResult.fromJson(response.data);

  }

}