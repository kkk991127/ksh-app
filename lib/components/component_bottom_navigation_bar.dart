import 'package:dclost_app/pages/page_main.dart';
import 'package:flutter/material.dart';

class ComponentBottomNavigationBar extends StatefulWidget {
  const ComponentBottomNavigationBar({super.key});

  @override
  State<ComponentBottomNavigationBar> createState() => _ComponentBottomNavigationBarState();
}

class _ComponentBottomNavigationBarState extends State<ComponentBottomNavigationBar> {
  int _bottomSelectedIndex = 0;

  // 바텀 네비게이션 바 공통 적용, 단 제품 상세보기 페이지에서는 컴포넌트 미적용.
  // 뒤로 가기 버튼, 마이페이지 버튼 미구현.
  final List<Widget> _widgetOptions = <Widget>[
    PageMain(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _bottomSelectedIndex = index;
    });
  }
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: _bottomSelectedIndex,
      onTap: _onItemTapped,
      type: BottomNavigationBarType.fixed,
      // 하단 바 스타일
      selectedItemColor: Colors.black87,
      unselectedItemColor: Colors.black38,
      items: [
        BottomNavigationBarItem(icon: BackButtonIcon(), label: '뒤로가기'),
        BottomNavigationBarItem(icon: Icon(Icons.home), label: '홈'),
        BottomNavigationBarItem(icon: Icon(Icons.store), label: '마이페이지'),
      ],
    );
  }
}
